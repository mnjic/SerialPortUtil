package com.ujn.spu.controller;

import com.ujn.spu.core.SerialPortManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class indexController {

    @Autowired
    SerialPortManager serialPortManager;

    @GetMapping({"/index", "/"})
    public String index(ModelMap mmap){
        List<String> portNames = serialPortManager.getPortNames();
        mmap.put("portNames", portNames);
        return "index";
    }
}
