package com.ujn.spu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SerialPortUtilApplication {

    public static void main(String[] args) {
        SpringApplication.run(SerialPortUtilApplication.class, args);
    }

}
