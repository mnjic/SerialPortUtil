var socket;


/*当前无可用设备*/
if ($("#portList").children().length <= 0){
    $("#openDevice").attr('disabled',"disabled");
    $("#closeDevice").attr('disabled',"disabled");
}

/*查看设备开启状态*/
var deviceIsOpen = sessionStorage.key("deviceIsOpen");

$(function(){
    //构建WebSocket
    if (typeof (WebSocket) == "undefined"){
        console.log("您的浏览器不支持WebSocket")
    }else {
        console.log("您的浏览器支持WebSocket")
        var socketUrl = 'ws://localhost:8080/serialport/api/websocket'

        if (socket != null){
            socket.close();
            socket = null;
        }

        socket = new WebSocket(socketUrl);

        //打开事件
        socket.onopen = function () {}

        //获取消息事件
        socket.onmessage = function (messageEvent){
            addMsg(messageEvent.data);
        }

        //关闭事件
        socket.onclose = function () {
            console.log("webSocket已关闭");
        }

        //发生错误事件
        socket.onerror = function () {
            console.log("webSocket发生了错误");
        }
    }

    //根据设备开启状态设置按钮点击
    if (deviceIsOpen != null){
        if (deviceIsOpen === "open"){
            $("#openDevice").removeAttr('disabled',"disabled");
            $("#closeDevice").attr('disabled',"disabled");
        }else{
            $("#openDevice").attr('disabled',"disabled");
            $("#closeDevice").removeAttr('disabled',"disabled");
        }
    }
    //打开设备
    $("#openDevice").click(function () {
        openLoadLayer("正在打开设备......");
        var portName = $("#portList").val();
        var options = {
            type: 'get',
            url: 'opt/port/open/' + portName,
            success:function (result) {
                addMsg(result.msg)
                //成功打开串口
                if (result.code === 0){
                    deviceIsOpen = "open";
                    sessionStorage.setItem("deviceIsOpen", deviceIsOpen);
                    $("#openDevice").attr('disabled',"disabled");
                    $("#closeDevice").removeAttr('disabled',"disabled");
                }
                layer.close(loadLayer)
            }
        };
        //异步打开串口
        $.ajax(options);
    })

    //关闭设备
    $("#closeDevice").click(function () {
        openLoadLayer('正在关闭设备......');
        var portName = $("#portList").val();
        var options = {
            type: 'get',
            url: 'opt/port/close/' + portName,
            success:function (result) {
                addMsg(result.msg)
                if (result.code === 0){
                    sessionStorage.removeItem("deviceIsOpen");
                    $("#openDevice").removeAttr('disabled',"disabled");
                    $("#closeDevice").attr('disabled',"disabled");
                }
                layer.close(loadLayer)
            }
        };
        //异步关闭串口
        $.ajax(options);
    })

    //发送信息
    $("#sendText").click(function(){
        //判断消息不能为空
        var msg = $("#sendTextArea").val();
        if (msg == null || msg === ''){
            layer.msg("发送信息不能为空！")
            return ;
        }

        openLoadLayer("正在发送信息.......");
        var portName = $("#portList").val();
        var option = {
            type    :  'post',
            url     :  'opt/port/send/' + portName,
            data    :  {
                msg : $("#sendTextArea").val()
            },
            success : function (result) {
                //清空内容
                $("#sendTextArea").val('')
                layer.close(loadLayer)
            }
        }
        $.ajax(option);
    })
})

/**
 * 添加显示信息
 * @param msg
 */
function addMsg(msg) {
    $("#infoTextArea").append(msg + '&#10;');
    var obj = document.getElementById("infoTextArea");
    obj.scrollTop = obj.scrollHeight; // good
}

/**
 * 添加遮罩层
 * @param msg
 */
function openLoadLayer(msg) {
    loadLayer= layer.msg(msg, {
        icon: 16,
        shade: [0.3, '#393D49'],
        time:'-1'
    });
}